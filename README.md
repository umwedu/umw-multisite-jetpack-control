# UMW Multisite JetPack Control #
**Contributors:** cgrymala

**Donate link:** http://giving.umw.edu/

**Tags:** jetpack, modules, multisite, control, deactivate

**Requires at least:** 3.9.1

**Tested up to:** 4.1.1

**Stable tag:** 1.2

**License:** GPLv2 or later

**License URI:** http://www.gnu.org/licenses/gpl-2.0.html


Allows code-based control over which JetPack modules are available for use on individual sites.

## Description ##

This plugin removes specific JetPack modules from the system so that individual site admins can not activate/enable them. The following modules have been disabled:

* [Contact Form](http://jetpack.me/support/contact-form/) - No need when we have Gravity Forms installed
* [Custom Content Types](http://jetpack.me/support/custom-content-types/) - Currently useless
* [Gravatar Hovercards](http://jetpack.me/support/gravatar-hovercards/) - Causes some JS conflicts throughout the site
* [Infinite Scroll](http://jetpack.me/support/infinite-scroll/) - None of our theme configurations support this
* [JetPack Single Sign-On](http://jetpack.me/support/sso/) - Would conflict with our sign-on system
* [Manage](http://jetpack.me/support/site-management/) - Would be ridiculous with the number of sites running off of one install
* [Mobile Theme](http://jetpack.me/support/mobile-theme/) - We don't want a third-party mobile theme, other than the WPTouch Pro theme we're running on a handful of sites
* [Monitor](http://jetpack.me/support/monitor/) - We have other systems in-place to keep an eye on site uptime
* [Notifications](http://jetpack.me/support/toolbar-notifications/) - We don't want to receive notifications about every little thing that happens throughout the install
* [Photon](http://jetpack.me/support/photon/) - Until we have a better chance to investigate how this effects us, we need it turned off
* [Post By Email](http://jetpack.me/support/post-by-email/) - Absolutely not
* [Protect](http://jetpack.me/support/security-features/) - The shared IP across campus was causing false-positive security flags
* [Site Icon](http://jetpack.me/support/site-icon/) - We have a single favicon that needs to be used
* [Site Verification](http://jetpack.me/support/site-verification-tools/) - We don't want them to verify that someone other than us owns the site
* [VideoPress](http://jetpack.me/support/videopress/) - We don't have a VideoPress subscription, so need to confuse users
* [VaultPress](http://jetpack.me/support/vaultpress/) - We don't have a subscription for this, either
* [WP.me Shortlinks](http://jetpack.me/support/wp-me-shortlinks/) - These were unreliable, and caused SiteImprove to throw a lot of Broken Link warnings.

The plugin also allows you to choose specific JetPack modules that will be forcibly activated. So far, the list of forcibly active modules is:

* [Site Stats](http://jetpack.me/support/wordpress-com-stats/)
* [Tiled Galleries](http://jetpack.me/support/tiled-galleries/)

## Installation ##

This plugin should only be installed as a mu-plugin

1. Upload `class-umw-multisite-jetpack-control.php` to the `/wp-content/mu-plugins/` directory
1. Do not upload any of the other files within this folder to the mu-plugins directory

## Frequently Asked Questions ##

### Where did this code come from? ###

The code used in this plugin was inspired by [a post by Jeremy Herve](http://jeremy.hu/customize-the-list-of-modules-available-in-jetpack/)

### How do we change the list of modules that are available to site admins? ###

There are no dashboard options to do this. However, you can use the `umw-blocked-jetpack-modules` filter to modify the list of modules that gets blacklisted. It's an auto-indexed array containing the slugs of the modules we want to block.

You can also use the `umw-forced-jetpack-modules` filter to modify the list of modules that are forcibly activated.

### Why is a module in my list of forced modules not being activated? ###

More than likely, that module is also in the blocked modules list. If a module appears in both lists, the blacklist will take precedence over the force list.

### Why is [insert module name here] included in the list of unavailable modules? ###

The explanations are included in the readme information above.

## Changelog ##

### 1.2 ###
* Add the list of modules that can be forcibly activated
* Add the `umw-forced-jetpack-modules` filter

### 1.1 ###
* Add the WP.me Shortlinks to the list of blocked modules

### 1.0 ###
* Add the `umw-blocked-jetpack-modules` filter to allow the list of blocked modules to be modified outside of the plugin

### 0.3 ###
* Add vaultpress to the list of blocked modules
* Force deactivate any blocked modules

### 0.2 ###
* Add blocked modules to the list of available modules (previous version just did default modules)

### 0.1 ###
* This is the initial version
